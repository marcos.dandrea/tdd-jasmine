"use strict";
const webpack = require('webpack');
module.exports = {
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ],
    entry: { jasmineloader: ["./test/lib/jasmine-loader.ts"] },
    output: {
        filename: "./build/js/[name].js"
    },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.tsx?$/
                , loader: "ts-loader"
                , exclude: /node_modules/
            },
            {
                test: /\.js$/,
                enforce: 'pre',
                use: ['source-map-loader'],
            },
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    devServer: {
        contentBase: ".",
        host: "localhost",
        port: 9000,
        inline: true,
        hot: true,
    },
};