
import { of, Observable, concat, from, interval, forkJoin, zip, combineLatest } from "rxjs";
import { delay, concatAll, first, mergeAll, concatMap, mergeMap, flatMap, switchMap, bufferTime, last, debounceTime, auditTime, distinctUntilChanged, map, catchError, take, exhaustMap } from 'rxjs/internal/operators';

describe("RXJS", () => {
    //Reactive programming is an asynchronous programming paradigm concerned 
    //with data streams and the propagation of change  

    //RxJS (Reactive Extensions for JavaScript) is a library for reactive programming 
    //using observables that makes it easier to compose asynchronous or callback-based code.
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    beforeEach(function () {

    });
    it("what observables are?", (done) => {
        //---https://www.freecodecamp.org/news/what-are-observables-how-they-are-different-from-promises/#:~:text=In%20short%2C%20you%20can%20say,time%2C%20either%20synchronously%20or%20asynchronously.
        //--- handling async requests
        //--- Observables are lazy Push collections of multiple values.
        //--- multiple values called streams (a stream is a sequence of data over time).
        //--- Lazy: lazy in the sense that they only execute values when something subscribes to it
        //-- are responsible for handling async requests. 
        //-- observables are simply a function that are able to give multiple values over time
        //---Observables are a part of the RXJS library. 
        var observable = Observable.create((observer: any) => {
            observer.next("Hi Observable");
            setTimeout(() => {
                observer.next("Yes, somehow understandable!")
                expect(observer).not.toBeNull()
                done()
            }, 1000)
            observer.next("Am I understandable?");
        })
        //----The observable doen't emit until an subscription is done (Lazy)
        observable.subscribe((result) => {
            console.log(result)
        })
    })
    it("Observables phases", (done) => {
        //--I - creation: is done using a create function.
        var observable = Observable.create((observer: any) => {
            observer.next("Hi Observable");
            throw new Error("error from observable")
        })
        //--II - subscription and execution
        const subscription = observable.subscribe((result) => {
            console.log(result)
        }, (error) => {
            console.log(error)
            expect(error).toBeTruthy();
            done()
        }, () => {
            console.log("completed")
        })
        //--III- destruction:
        subscription.unsubscribe();
    })
    it("Operators - map", (done) => {
        //applies a projection to each value and emits that
        // projection in the output Observable.
        const obs = Observable.create((observer: any) => {
            let item = 0;
            for (var n = 0; n < 3; n++) {
                setTimeout(() => {
                    item++;
                    observer.next(item)
                }, getRandomInt(5, 10))
            }
        }) as Observable<any>;
        let result = 0
        obs.pipe(map((x) => {
            return x * 10
        }
        )).subscribe(
            y => {
                console.log(y);
                result = result + (y as any);
            }
        )

        setTimeout(() => { expect(result).toBe(60); done(); }, 100)
    });
    it("Operators - from", (done) => {
        //-----convert various other objects and data types into Observables
        let result = 0
        from([1, 2, 3, 4, 5]).subscribe(x => {
            result = result + x
            console.log(x);
        })
        setTimeout(() => { expect(result).toBe(15); done(); }, 500)
    });
    it("Operators - ConcatAll", (done) => {
        //---Joins every Observable emitted by the source (a higher-order Observable).
        //It subscribes to each inner Observable only after the previous inner Observable has completed, and merges all of their values into the returned observable.
        const sourceOfObservables = Observable.create((observer: any) => {
            var n = 0;
            var innerCount = 0;
            for (n = 0; n < 6; n++) {
                observer.next(Observable.create((observer: any) => {
                    setTimeout(() => {
                        innerCount++;
                        observer.next(innerCount)
                        observer.complete() //if inner observable is not complete, concatAll will not subscribe to next inner observable. WARNING: This can run into memory leaks 
                    }, 20);
                }));
            }
        }) as Observable<any>
        const firstOrder = sourceOfObservables.pipe(concatAll()); //MergeAll: subscribe to the next observable even the previous has not been completed,
        const result = [];
        firstOrder.subscribe(x => { console.log(x); result.push(x) });
        //---------------------------------------------
        setTimeout(() => { expect(result.length).toBe(6); done(); }, 500)
        //Warning: If the source Observable emits Observables quickly and endlessly,
        // and the inner Observables it emits generally complete slower than the source emits,
        // you can run into memory issues as the incoming Observables collect in an unbounded buffer.
    })
    it("Operators - MergeAll", (done) => {
        //Converts a higher-order Observable (Observable that emits other Observables) into a first-order Observable 
        //which concurrently delivers all values that are emitted on the inner Observables.        
        const observable1 = Observable.create((observer: any) => {
            for (var n = 0; n < 6; n++) {
                setTimeout(() => {
                    console.log("A1")
                    observer.next("A1")
                    //observer.complete(): TODO: show difference with concat
                }, getRandomInt(5, 10))
            }
        });
        const observable2 = Observable.create((observer: any) => {
            for (var n = 0; n < 3; n++) {
                setTimeout(() => {
                    console.log("A2")
                    observer.next("A2")
                }, getRandomInt(5, 10))
            }
        });
        const observable3 = Observable.create((observer: any) => {
            console.log("A3")
            observer.next("A3")
        });
        console.log("MergeAll")
        const higherOderObservable = from([observable1, observable2, observable3]);
        const result = [];
        higherOderObservable.pipe(mergeAll()) //ConcatAll: subscribe to the next observable only when the previous has been completed
            .subscribe(x => {
                console.log(x);
                result.push(x);
            });
        setTimeout(() => {
            expect(result.length).toBe(10);
            done()
        }, 100);
    })
    it("Operators - ConcatMap", (done) => {
        //Map values to inner observable, subscribe and emit in order.  
        //subscribe to the next observable only when the previous has been completed
        const createsObservable = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item)
                observer.complete()
            });
        }
        let result = 0;
        from([1, 2, 3, 4, 5]).pipe(concatMap(x => createsObservable(x))).subscribe(x => {
            result = result + (x as any);
            console.log(x);
        })
        setTimeout(() => {
            expect(result).toEqual(15);
            done()
        }, 100);
    })
    it("Operators - MergeMap", (done) => {
        //Map values to inner observable, subscribe and emit
        //subscribe to the next observable even if the previous has not been completed
        const createsObservable = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item)
            });
        }
        let result = 0;
        from([1, 2, 3, 4, 5]).pipe(mergeMap(x => createsObservable(x))).subscribe(x => {
            result = result + (x as any);
            console.log(x);
        })
        setTimeout(() => {
            expect(result).toEqual(15);
            done()
        }, 100);
    })
    it("Operators - flatMap", (done) => {
        //flatMap is an alias for mergeMap!
        const createsObservable = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item)
            });
        }
        let result = 0;
        from([1, 2, 3, 4, 5]).pipe(flatMap(x => createsObservable(x))).subscribe(x => {
            console.log(x)
            result = result + (x as any);
        })
        setTimeout(() => {
            expect(result).toEqual(15);
            done()
        }, 100);
    })
    it("Operators - switchMap", (done) => {
        //Map to observable, complete previous inner observable, emit values.
        const observable = Observable.create((observer: any) => {
            let n = 0;
            let item = 0;
            for (n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next(item)
                }, 50)
            }
        });
        const createsObservable = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item + "obser")
            }).pipe(delay(100));
        }
        observable.pipe(switchMap(x => createsObservable(x))).subscribe(x => {
            console.log(x);
            expect(x).toBe("6obser");
              //-------Observables from 1 to 5 are discarded. SwitchMap "switch" the observable
            done()
        })
        //----TODO: show difference with mergeMap. 
    })
    it('Operators auditTime + distinctUntilChanged', (done) => {
        // When you are interested in ignoring a source observable for a given amount of time,
        // you can use auditTime. A possible use case is to only emit certain events (i.e. mouse clicks) at a maximum rate per second.
        // After the specified duration has passed, the timer is disabled and the most recent source value is emitted on the output Observable, and this process repeats for the next source value.
        //The difference with debounceTime is that debounceTime rest the timer every time an observable is emitted. auditTime will emit the value after a period of time
        const userInputText = Observable.create((observer: any) => {
            let n = 0;
            let item = 0;
            for (n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next('A' + item)
                }, 150 + item)

                setTimeout(() => {
                    item++
                    observer.next('B' + item)
                }, 350 + item)

                setTimeout(() => {
                    item++
                    observer.next('B' + item)
                }, 500 + item)
            }
        }) as Observable<any>;

        const httpPost = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item)
            }).pipe(delay(400));
        }
        let result1 = 0;
        userInputText.pipe(flatMap((x => httpPost(x)))).subscribe(x => {
            result1 = result1 + 1;
            console.log("http://example.com/" + x);
        });

        //   userInputText.pipe(auditTime(200),flatMap((x=> httpPost(x)))).subscribe(x => {
        //      result1 = result1 + 1;
        //      console.log("http://example.com/" + x);
        //  });

        // userInputText.pipe(auditTime(200), distinctUntilChanged(), switchMap((x => httpPost(x)))).subscribe(x => {
        //     result1 = result1 + 1;
        //     console.log("http://example.com/" + x);
        // });
        setTimeout(() => {
            expect(result1).toEqual(18);
            //expect(result1).toEqual(2)
            //expect(result1).toEqual(1)
            done()
        }, 2000);
    });
    it("Operators - forkjoin", (done) => {
        //When all observables complete, emit the last emitted value from each.
        const fakeHttp1 = Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next("JSONResult1")
                observer.complete() //--TODO: observable must complete
            }, getRandomInt(5, 10))
        });
        const fakeHttp2 = Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next("JSONResult2")
                observer.complete()
            }, getRandomInt(5, 10))
        });

        forkJoin({ http1: fakeHttp1, http2: fakeHttp2 })
            .subscribe(x => {
                console.log(x)
                const result = { http1: "JSONResult1", http2: "JSONResult2" }
                expect(x).toEqual(result);
                done()
            });
    });
    it("Operators - forkjoin", (done) => {
        //When all observables complete, emit the last emitted value from each.
        const fakeHttp1 = Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next("JSONResult1")
                observer.complete() //--TODO: observable must complete
            }, getRandomInt(5, 10))
        });
        const fakeHttp2 = Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next("JSONResult2")
                observer.complete()
            }, getRandomInt(5, 10))
        });

        forkJoin({ http1: fakeHttp1, http2: fakeHttp2 })
            .subscribe(x => {
                console.log(x)
                const result = { http1: "JSONResult1", http2: "JSONResult2" }
                expect(x).toEqual(result);
                done()
            });
    });
    it("Operators - zip", (done) => {
        //--- After all observables emit, emit values as an array
        const fakeSender1 = Observable.create((observer: any) => {
            let item = 0
            for (var n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next("JSONResult" + item)
                }, getRandomInt(5, 10))
            }
        });
        const fakeSender2 = Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next("JSONResult2")
            }, getRandomInt(5, 10))
        });

        zip(fakeSender1, fakeSender2)
            .subscribe(x => {
                console.log(x)
                const result = ["JSONResult1", "JSONResult2"]
                expect(x).toEqual(result);
                done()
            });
    });
    it("Operators - combineLatest", (done) => {
        //---When any observable emits a value, emit the last emitted value from each.
        const fakeSender1 = Observable.create((observer: any) => {
            let item = 0;
            for (var n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next("JSONResult" + item)
                }, getRandomInt(5, 10))
            }
        });
        const fakeSender2 = Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next("JSONResult2")
            }, getRandomInt(5, 10))
        });
        const result = []
        combineLatest(fakeSender1, fakeSender2)
            .subscribe(x => {
                result.push(x)
                console.log(x)
            });
        setTimeout(() => {
            const last = ["JSONResult6", "JSONResult2"]
            expect(result.pop()).toEqual(last);
            done()
        }, 100);
    });
    it("Operators - take", (done) => {
        //---Emit provided number of values before completing.
        const fakeSender2 = Observable.create((observer: any) => {
           let item = 0;
            for (var n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next(item)
                }, getRandomInt(1, 3))
            }
        });
        let result = 0;
        fakeSender2.pipe(take(2)).subscribe(x=> {
            result = result +1;
            console.log(x);

        }, () =>{ console.log("error")}, () =>{ console.log("completed")}) 
        setTimeout(() => { expect(result).toBe(2); done(); }, 20)
 
    });
    it("Operators - catch", (done) => {
        // Gracefully handle errors in an observable sequence.
        const obs = Observable.create((observer: any) => {
            //observer.next("item")
            throw new Error()
        }) as Observable<any>;
        obs.pipe(catchError((x) => {
            return of("upss error")
        }
        )).subscribe(
            y => {
                console.log(y);
                expect(y).toBe("upss error"); done();
            }
        )
    });
    it("Operators - exhaustMap", (done) => {
        //Map to inner observable, ignore other values until that observable completes
        const observable = Observable.create((observer: any) => {
            let n = 0;
            let item = 0;
            for (n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next(item)
                }, 50)
            }
        });
        const createsObservable = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item + "obser")
            }).pipe(delay(100));
        }
        observable.pipe(exhaustMap(x => createsObservable(x))).subscribe(x => {
            console.log(x);
            expect(x).toBe("1obser");
            //-------Observables from 2 to 6 are ignore because the first one is not resolved
            done()
        })
        //----TODO: show difference with switchMap. 
    })
    it('Operators debounceTime + distinctUntilChanged', (done) => {
        //debounceTime delays the values emitted by a source for the given due time.
        //The difference with debounceTime is that debounceTime rest the timer every time an observable is emitted. auditTime will emit the value after a period of time
        const userInputText = Observable.create((observer: any) => {
            let n = 0;
            let item = 0;
            for (n = 0; n < 6; n++) {
                setTimeout(() => {
                    item++
                    observer.next('A' + item)
                }, 150 + item)

                setTimeout(() => {
                    item++
                    observer.next('B' + item)
                }, 200 + item)

                setTimeout(() => {
                    item++
                    observer.next('C' + item)
                }, 250 + item)

                setTimeout(() => {
                    item++
                    observer.next('D' + item)
                }, 300 + item)
            }
        }) as Observable<any>;

        const httpPost = function (item) {
            return Observable.create((observer: any) => {
                observer.next(item)
            }).pipe(delay(400));
        }
        let result1 = 0;
         userInputText.pipe(debounceTime(100), distinctUntilChanged(), switchMap((x => httpPost(x)))).subscribe(x => {
             result1 = result1 + 1;
             console.log("http://example.com/" + x);
         });
        setTimeout(() => {
            expect(result1).toEqual(1)
            done()
        }, 2000);
    });
});