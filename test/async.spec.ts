 
import { of } from "rxjs";
import { delay } from 'rxjs/internal/operators';
import { HeroComponent } from "../src/hero.component";

describe("first test", () => {
    beforeEach(function () {

    });
    it("javascript is single thread", () => {
        //--'Asynchrony' in the computer world means that the flow of the program occurs independently.
        //--It does not wait for a task to get finished. It moves to the next task.
        //---The co-worker handles those unfinished tasks. In the background, a co-worker works and handles those unfinished tasks and once they are done, it sends data back.
        //--how we handle the data that is returned. The answer is Promises, Observables, callbacks and many more.
        let loop = false;
        setTimeout(() => {
            loop = false;  // callback execution is unreachable because main thread is still busy with below infinite loop code. 
            console.log('set timeout');
        });
        while (loop) {
            console.log('loop', loop);   // Infinite loop 
        }
        expect(1).toEqual(1)  // Unreachable code 
    })
    xit("testing promise wrong way", () => {
        //---Una Promise rappresenta un'operazione che non è ancora completata, 
        const myHttpService = {
            get: function (message: String) {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                         resolve({ result: message })
                    },10)
                })
            }
        };
        let result = null;
        myHttpService.get("ok").then((resolvedResult) => {
            result = resolvedResult
            expect(result).toEqual({ result: "gotoerror" })
        })
        expect(1).toBe(1)
    })
    it("testing promise with done", (done) => {
        const myHttpService = {
            get: function (message: String) {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                         resolve({ result: message })
                    },1000)
                })
            }
        };
        let result = null;
        myHttpService.get("ok").then((resolvedResult) => {
            result = resolvedResult
            expect(result).toEqual({ result: "ok" })
            done()
        })
    })
    it("testing promise with async", async() => {
        const myHttpService = {
            get: function (message: String) {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                         resolve({ result: message })
                    },1000)
                })
            }
        };
        const result = await myHttpService.get("ok")
        expect(result).toEqual({ result: "ok" })
    })
    xit("common error testing observables in angular", () => {
        const mockHttp = {get:function(url){
            return of({name:"batman"}).pipe(delay(100))
        }}
        const component = new HeroComponent(mockHttp);
        component.ngOnInit();
        expect(component.myHeroName).toEqual("batman")
    })
});