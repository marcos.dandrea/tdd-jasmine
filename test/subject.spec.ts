import { Subject } from "rxjs/internal/Subject";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { ReplaySubject } from "rxjs/internal/ReplaySubject";
import { AsyncSubject } from "rxjs/internal/AsyncSubject";
 


describe("RXJS - subjects", () => {
    beforeEach(function () {

    });
    it("what subjects are?", (done) => {
        // What is a Subject? An RxJS Subject is a special type of Observable 
        // that allows values to be multicasted to many Observers.
        const subject = new Subject<number>();
        let result = 0
        subject.subscribe(x => {
            result = result + x;
            console.log(x)
        });
        subject.subscribe(x => {
            result = result + x;
            console.log(x)
        });
        subject.next(1);
        subject.next(2);
        setTimeout(() => { expect(result).toBe(6); done(); }, 20)
    });
    it("BehaviorSubject", (done) => {
        //One of the variants of Subjects is the BehaviorSubject, which has a notion of "the current value". 
        //It stores the latest value emitted to its consumers, and whenever a new Observer subscribes,
        //it will immediately receive the "current value" from the BehaviorSubject.
        const subject = new BehaviorSubject<number>(10);
        let result = 0
        subject.next(1);
        subject.next(2);
        subject.subscribe(x => {
            result = result + 10;
            console.log(x)
        });
        setTimeout(() => { expect(result).toBe(10); done(); }, 20)
    });
    it("ReplaySubject", (done) => {
        //A ReplaySubject is similar to a BehaviorSubject in that it can send old values to new subscribers,
        // but it can also record a part of the Observable execution.
        const subject = new ReplaySubject<number>(3);
        subject.next(1);
        subject.next(2);
        subject.next(3);
        subject.next(4);
        subject.next(5);
        let result = 0
        subject.subscribe(x => {
            result = result + x;
            console.log(x)
        });
        subject.subscribe(x => {
            result = result + x;
            console.log(x)
        });
        setTimeout(() => { expect(result).toBe(24); done(); }, 20)
    });
    it("AsyncSubject", (done) => {
        //The AsyncSubject is a variant where only the last value of the Observable execution is sent to its observers,
        // and only when the execution completes.
        const subject = new AsyncSubject();
        subject.next(1);
        subject.next(2);
        subject.next(3);
        subject.next(4);
        subject.next(5);
        let result = 0
        subject.subscribe(x => {
            result = result + (x as any);
            console.log(x)
        });
        subject.complete() //----TODO: Test without complete
        setTimeout(() => { expect(result).toBe(5); done(); }, 20)
    });

});