declare global {
    interface Window {
        jasmineRequire: any;
        jasmineRef: any;
      }
}
import * as jasmineRequire from 'jasmine-core/lib/jasmine-core/jasmine.js';
window.jasmineRequire = jasmineRequire;
import 'jasmine-core/lib/jasmine-core/jasmine-html.js';
import 'jasmine-core/lib/jasmine-core/boot.js';
//---ADD specs
import '../async.spec';
import '../rxjs.spec'
import '../subject.spec'