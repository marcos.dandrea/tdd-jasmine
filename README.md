# TDD-Jasmine
- Start server: npm run start
- Open Test Runner Page: http://localhost:9000/test/lib/test-runner.html


# Add Spec
- Add new spec file to test folder
- Add import to test/lib/jasmine-loader.ts

# References
https://www.learnrxjs.io/learn-rxjs/operators/transformation/switchmap
https://rxjs-dev.firebaseapp.com/api/operators/switchMap
https://www.freecodecamp.org/news/what-are-observables-how-they-are-different-from-promises/
  

Marcos Ariza D'Andrea
dandrea.marcos@gmail.com