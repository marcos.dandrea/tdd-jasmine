export class TestSample {
    constructor(private welcomeMessage: string, private name: string){

    }
    getWelcomeMessage(){
        return this.welcomeMessage + " " + this.name;
    }
}