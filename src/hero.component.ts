export class HeroComponent {
    constructor(private httpService: any) {

    }
    public myHero = null;
    public myHeroName = null;
    ngOnInit() {
        this.httpService.get("url")
            .subscribe(result => {
                this.myHero = result;
            })
        this.myHeroName = this.myHero.name;
    }
}